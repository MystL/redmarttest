package com.vin.redmarttest.data;

import junit.framework.TestCase;

import static org.junit.Assert.*;

/**
 * Created by melvin on 13/4/18.
 */
public class PricingTest extends TestCase{

    public void testInitPricing_validButZero(){
        Pricing pricing = new Pricing(0.0);
        assertFalse(pricing.isValid());
    }

    public void testInitPricing_validAndNotZero(){
        Pricing pricing = new Pricing(10.0);
        assertTrue(pricing.isValid());
    }

    public void testInitPricing_invalid(){
        Pricing pricing = new Pricing(-0);
        assertFalse(pricing.isValid());
    }

}