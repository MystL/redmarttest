package com.vin.redmarttest.data;

import com.vin.redmarttest.exceptions.ConfigParserException;

import junit.framework.TestCase;

import java.util.List;

public class ProductJsonMarshallerTest extends TestCase{

    private ProductJsonMarshaller marshaller;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        marshaller = new ProductJsonMarshaller();
    }

    public void testToJson_null(){
        try{
            marshaller.toJson(null);
            fail("should throw exception");
        }catch (ConfigParserException e){
            //...
        }
    }

    public void testFromJson_valid(){
        String jsonString = "{\"id\":33973,\"title\":\"Australian Broccoli\",\"desc\":\"Product of Australia. Versatile broccoli is delicious raw or cooked, and it is packed with so many complex nutrients that it's often called a superfood.\",\"images\":[{\"h\":0,\"w\":0,\"name\":\"/i/m/8885003328014_0005_1520495600715.jpg\",\"position\":0},{\"h\":0,\"w\":0,\"name\":\"/i/m/8885003328014_0006_1520495611293.jpg\",\"position\":1}],\"pricing\":{\"price\":2.6}}";

        Product expected = marshaller.fromJson(jsonString);
        assertEquals("33973", expected.getId());
        assertEquals("Australian Broccoli", expected.getTitle());
        assertEquals("Product of Australia. Versatile broccoli is delicious raw or cooked, and it is packed with so many complex nutrients that it's often called a superfood.", expected.getDescription());
        assertEquals(new Pricing(2.6), expected.getPricing());
        assertEquals(new Image(0,0,"/i/m/8885003328014_0005_1520495600715.jpg", 0), expected.getImages()[0]);
        assertEquals(new Image(0,0,"/i/m/8885003328014_0006_1520495611293.jpg",1), expected.getImages()[1]);
    }

    public void testRoundTrip(){

        Product product = new Product("33973", "Australian Broccoli", "Product of Australia. Versatile broccoli is delicious raw or cooked, and it is packed with so many complex nutrients that it's often called a superfood.",
                new Pricing(2.6), new Image[]{new Image(0,0,"/i/m/8885003328014_0005_1520495600715.jpg", 0),
                new Image(0,0,"/i/m/8885003328014_0006_1520495611293.jpg",1)});

        assertEquals(product, marshaller.fromJson(marshaller.toJson(product)));
    }
}