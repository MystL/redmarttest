package com.vin.redmarttest.data;

import junit.framework.TestCase;

public class ProductTest extends TestCase {

    public void testInit_allNull() {
        Product product = new Product(null, null, null, null, null);
        assertFalse(product.isValid());
    }

    public void testInit_validParams() {
        Product product = new Product("someid", "sometitle", "somedesc", new Pricing(2.0), new Image[]{new Image(0, 0, "someurl", 0)});
        assertTrue(product.isValid());
    }

    public void testProduct_noId() {
        Product product = new Product(null, "sometitle", "somedesc", new Pricing(2.0), new Image[]{new Image(0, 0, "someurl", 0)});
        assertFalse(product.isValid());
    }

    public void testProduct_noTitle() {
        Product product = new Product("someid", null, "somedesc", new Pricing(2.0), new Image[]{new Image(0, 0, "someurl", 0)});
        assertFalse(product.isValid());
    }

    public void testProduct_noPricing() {
        Product product = new Product("someid", "sometitle", "somedesc", null, new Image[]{new Image(0, 0, "someurl", 0)});
        assertFalse(product.isValid());
    }

    public void testProduct_noImages() {
        Product product = new Product("someid", "sometitle", "somedesc", new Pricing(2.0), null);
        assertTrue(product.isValid());
    }

    public void testProduct_getImages_null() {
        Product product = new Product("someid", "sometitle", "somedesc", new Pricing(2.0), null);
        assertEquals(0, product.getImages().length);

    }

}