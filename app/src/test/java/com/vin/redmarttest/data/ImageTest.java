package com.vin.redmarttest.data;

import junit.framework.TestCase;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * Created by melvin on 13/4/18.
 */
public class ImageTest extends TestCase{

    public void testInit_allValid(){
        Image image = new Image(0,0,"/somepartialurl",0);
        assertTrue(image.isValid());
    }

    public void testInit_nameNull(){
        Image image = new Image(0,0,null,0);
        assertFalse(image.isValid());
    }

    public void testGetFullImageUrl() throws MalformedURLException {
        Image image = new Image(0,0,"/somepartialurl",0);
        URL expectedUrl = new URL("http://media.redmart.com/newmedia/200p/somepartialurl");
        assertEquals(expectedUrl, image.getFullImageUrl());
    }

    public void testGetFullImageUrl_ifNameHasUnexpectedPrefix() throws MalformedURLException {
        Image image = new Image(0,0,"somepartialurl",0);
        URL expectedUrl = new URL("http://media.redmart.com/newmedia/200p/somepartialurl");
        assertEquals(expectedUrl, image.getFullImageUrl());
    }

    public void testGetFullImageUrl_null(){
        Image image = new Image(0,0, null, 0);
        assertNull(image.getFullImageUrl());
    }

    public void testGetFullImageUrl_empty(){
        Image image = new Image(0,0, "", 0);
        assertNull(image.getFullImageUrl());
    }

}