package com.vin.redmarttest.data;

import com.vin.redmarttest.Validatable;

public class Pricing implements Validatable {

    private double price = -1;

    public Pricing(double price){
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public boolean isValid() {
        if(price > 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pricing pricing = (Pricing) o;

        return Double.compare(pricing.price, price) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(price);
        return (int) (temp ^ (temp >>> 32));
    }
}
