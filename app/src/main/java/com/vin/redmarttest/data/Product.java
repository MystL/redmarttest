package com.vin.redmarttest.data;

import com.vin.redmarttest.Validatable;

import java.util.Arrays;

public class Product implements Validatable {

    private String id = "";
    private String title = "";
    private String desc = "";
    private Pricing pricing;
    private Image[] images;

    public Product(String id, String title, String desc, Pricing pricing, Image[] images) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.pricing = pricing;
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return desc;
    }

    public Pricing getPricing() {
        return pricing;
    }

    public Image[] getImages() {
        if (images != null) {
            return images;
        }
        return new Image[0];
    }

    @Override
    public boolean isValid() {
        if (id != null && !id.isEmpty()
                && title != null && !title.isEmpty()
                && pricing != null && pricing.isValid()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != null ? !id.equals(product.id) : product.id != null) return false;
        if (title != null ? !title.equals(product.title) : product.title != null) return false;
        if (desc != null ? !desc.equals(product.desc) : product.desc != null) return false;
        if (pricing != null ? !pricing.equals(product.pricing) : product.pricing != null)
            return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(images, product.images);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (desc != null ? desc.hashCode() : 0);
        result = 31 * result + (pricing != null ? pricing.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(images);
        return result;
    }
}
