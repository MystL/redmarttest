package com.vin.redmarttest.data;

import com.vin.redmarttest.Validatable;


public class ProductDetails implements Validatable {

    private Product product;

    public ProductDetails(Product product){
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    @Override
    public boolean isValid() {
        return product != null;
    }
}
