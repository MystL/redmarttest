package com.vin.redmarttest.data;

import com.vin.redmarttest.Validatable;

import java.net.MalformedURLException;
import java.net.URL;

public class Image implements Validatable{

    private static final String IMAGE_ENDPOINT_URL = "http://media.redmart.com/newmedia/200p";
    private int h;
    private int w;
    private String name = "";
    private int position;

    public Image(int h, int w, String name, int position){
        this.h = h;
        this.w = w;
        this.name = name;
        this.position = position;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public String getName() {
        return name;
    }

    public URL getFullImageUrl(){
        if(name != null && !name.isEmpty()){
            try {
                if(name.charAt(0) == '/'){
                    return new URL(IMAGE_ENDPOINT_URL.concat(name));
                }
                return new URL(IMAGE_ENDPOINT_URL + "/" + name);
            } catch (MalformedURLException e) {
                return null;
            }
        }
        return null;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public boolean isValid() {
        if(name != null && !name.isEmpty() && getFullImageUrl() != null){
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Image image = (Image) o;

        if (h != image.h) return false;
        if (w != image.w) return false;
        if (position != image.position) return false;
        return name != null ? name.equals(image.name) : image.name == null;
    }

    @Override
    public int hashCode() {
        int result = h;
        result = 31 * result + w;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + position;
        return result;
    }
}
