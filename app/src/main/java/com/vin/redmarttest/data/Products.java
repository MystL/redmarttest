package com.vin.redmarttest.data;

import com.vin.redmarttest.Validatable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Products implements Validatable {

    private Product[] products = new Product[0];

    public Products(Product[] products) {
        this.products = products;
    }

    public List<Product> getProductsList() {
        if (products != null && products.length > 0) {
            return Arrays.asList(products);
        }
        return new ArrayList<>();
    }

    @Override
    public boolean isValid() {
        if (products != null && products.length > 0) {
            return true;
        }
        return false;
    }
}
