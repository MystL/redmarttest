package com.vin.redmarttest.data;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.vin.redmarttest.JsonMarshaller;
import com.vin.redmarttest.exceptions.ConfigParserException;


public class ProductJsonMarshaller extends JsonMarshaller<Product> {

    private Gson gson = new Gson();

    @Override
    public Product fromJson(String jsonString) throws ConfigParserException {
        try {
            if(jsonString != null){
                return gson.fromJson(jsonString, Product.class);
            }
            throw new ConfigParserException("product json is null");
        } catch (JsonSyntaxException e) {
            throw new ConfigParserException(e);
        }

    }

    @Override
    public String toJson(Product item) throws ConfigParserException {
        if(item != null){
            return gson.toJson(item);
        }
        throw new ConfigParserException("product is null");
    }

}
