package com.vin.redmarttest.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by melvin on 14/4/18.
 */

public class ProductItemViewHolder extends RecyclerView.ViewHolder{

    private ImageView imageView;
    private TextView titleTextView;
    private TextView priceTextView;

    public ProductItemViewHolder(View itemView, ImageView imageView, TextView titleTextView, TextView priceTextView) {
        super(itemView);
        this.imageView = imageView;
        this.titleTextView = titleTextView;
        this.priceTextView = priceTextView;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getTitleTextView() {
        return titleTextView;
    }

    public TextView getPriceTextView() {
        return priceTextView;
    }
}
