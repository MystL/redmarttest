package com.vin.redmarttest.recyclerview;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by melvin on 14/4/18.
 */

public class SpaceItemDecorations extends RecyclerView.ItemDecoration{

    private int space;

    public SpaceItemDecorations(int space){
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = space;
        outRect.right = space;
        outRect.top = space * 2;
    }
}
