package com.vin.redmarttest.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vin.redmarttest.R;
import com.vin.redmarttest.data.Product;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by melvin on 14/4/18.
 */

public class ProductsListAdapter extends RecyclerView.Adapter<ProductItemViewHolder>{

    private List<Product> products;
    private Context context;
    private PublishSubject<Product> itemSelectedSubject;

    public ProductsListAdapter(Context context){
        this.context = context;
        products = new ArrayList<>();
        itemSelectedSubject = PublishSubject.create();
    }

    @Override
    public ProductItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.product_item_view, parent, false);
        ImageView imageView = v.findViewById(R.id.product_imgview);
        TextView titleTextView = v.findViewById(R.id.product_title_lbl);
        TextView priceTextView = v.findViewById(R.id.product_price);
        return new ProductItemViewHolder(v, imageView, titleTextView, priceTextView);
    }

    @Override
    public void onBindViewHolder(ProductItemViewHolder holder, int position) {
        final Product product = products.get(position);

        if(product.getImages().length > 0){
            URL imageUrl = product.getImages()[0].getFullImageUrl();
            if(imageUrl != null){
                Picasso.with(context).load(imageUrl.toString()).fit().centerInside().into(holder.getImageView());
            }
        }

        holder.getTitleTextView().setText(product.getTitle());
        holder.getPriceTextView().setText(String.format("%s%s", "$", product.getPricing().getPrice()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemSelectedSubject.onNext(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void setData(List<Product> productList){
        products.clear();
        products.addAll(productList);
        notifyDataSetChanged();
    }

    public Observable<Product> getSelectedProductObs(){
        return itemSelectedSubject.distinctUntilChanged();
    }

}
