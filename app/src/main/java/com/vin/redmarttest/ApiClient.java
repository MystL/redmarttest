package com.vin.redmarttest;

import android.content.Context;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.vin.redmarttest.data.ProductDetails;
import com.vin.redmarttest.data.Products;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public class ApiClient {

    public static final int CACHE_MAX_SIZE = 10 * 1024 * 1024;
    private final Api api;

    public ApiClient(Context context, URL apiEndpoint) {
        api = createRestAdapter(context, apiEndpoint).create(Api.class);
    }

    // For Testing purposes only
    public ApiClient(Api api) {
        this.api = api;
    }


    private RestAdapter createRestAdapter(Context context, URL apiEndpoint) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setCache(new Cache(context.getCacheDir(), CACHE_MAX_SIZE));
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);

        return new RestAdapter.Builder()
                .setEndpoint(apiEndpoint.toString())
                .setClient(new OkClient(okHttpClient))
                .build();
    }

    public Products getProductsForPage(int page, int pageSize){
        try {
            return api.getProductsForPage(page, pageSize);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ProductDetails getProductForId(String id){
        try{
            return api.getProductForId(id);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public interface Api {

        @GET("/v1.6.0/catalog/search")
        Products getProductsForPage(@Query("page") int page, @Query("pageSize") int pageSize);

        @GET("/v1.6.0/catalog/products/{product_id}")
        ProductDetails getProductForId(@Path("product_id") String product_id);
    }

}
