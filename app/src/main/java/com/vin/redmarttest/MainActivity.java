package com.vin.redmarttest;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.vin.redmarttest.data.Product;
import com.vin.redmarttest.data.Products;
import com.vin.redmarttest.exceptions.ApiException;
import com.vin.redmarttest.recyclerview.ProductsListAdapter;
import com.vin.redmarttest.recyclerview.SpaceItemDecorations;

import java.net.URL;
import java.util.List;
import java.util.Set;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

public class MainActivity extends AppCompatActivity {

    public static final String API_ENDPOINT = "https://api.redmart.com";
    private ApiClient apiClient;
    private ProductLoaderHelper productLoaderHelper;
    private int currentLoadedPage = -1;
    private RecyclerView recyclerView;
    private ProductsListAdapter adapter;
    private AlertDialog loadingProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new ProductsListAdapter(MainActivity.this);
        recyclerView = findViewById(R.id.products_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0){
                    int threshold = 2;
                    GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                    int lastItem = gridLayoutManager.findLastCompletelyVisibleItemPosition();
                    int currentTotalCount = gridLayoutManager.getItemCount();

                    if(currentTotalCount <= lastItem + threshold){
                        showProgressDialog();
                        loadNextPageOfData();
                    }
                }

            }
        });
        try {
            apiClient = new ApiClient(MainActivity.this, new URL(API_ENDPOINT));
            productLoaderHelper = new ProductLoaderHelper(apiClient);
            loadNextPageOfData();
        } catch (Exception e) {
            e.printStackTrace();
            //TODO Handle error
        }
    }

    private void loadNextPageOfData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                currentLoadedPage += 1;
                productLoaderHelper.loadNext(currentLoadedPage);
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();

        adapter.getSelectedProductObs().distinctUntilChanged().subscribe(new Subscriber<Product>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(Product product) {

                Intent intent = new Intent(MainActivity.this, ProductDetailsActivity.class);
                intent.putExtra(ProductDetailsActivity.PRODUCT_ID_KEY, product.getId());
                startActivity(intent);
            }
        });

        productLoaderHelper.getLoadedProductObservables().observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<List<Product>>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(List<Product> products) {
                if(products != null){
                    dismissProgressDialog();
                    adapter.setData(products);
                } else {
                    dismissProgressDialog();
                    Toast.makeText(getApplicationContext(), "Error loading Data, Please try again later", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void showProgressDialog(){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
        alertBuilder.setView(R.layout.loading_layout);
        alertBuilder.create();

        if(loadingProgressDialog != null){
            loadingProgressDialog.dismiss();
        }
        loadingProgressDialog = alertBuilder.show();
    }

    private void dismissProgressDialog(){
        if(loadingProgressDialog != null){
            loadingProgressDialog.dismiss();
        }
    }
}
