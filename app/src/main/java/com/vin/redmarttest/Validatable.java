package com.vin.redmarttest;

public interface Validatable {

    boolean isValid();
}
