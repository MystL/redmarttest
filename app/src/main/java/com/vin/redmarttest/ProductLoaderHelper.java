package com.vin.redmarttest;


import android.util.Log;

import com.vin.redmarttest.data.Product;
import com.vin.redmarttest.data.ProductDetails;
import com.vin.redmarttest.data.Products;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

public class ProductLoaderHelper {

    public static final int PAGE_SIZE = 20;
    private List<Product> currentProductsList;
    private BehaviorSubject<List<Product>> loadedProductsSubject;
    private BehaviorSubject<Product> productDetailsSubject;
    private ApiClient apiClient;

    public ProductLoaderHelper(ApiClient apiClient) {
        this.apiClient = apiClient;
        loadedProductsSubject = BehaviorSubject.create();
        productDetailsSubject = BehaviorSubject.create();
        currentProductsList = new ArrayList<>();
    }

    public void loadNext(int page) {
        try {
            Products responseProducts = apiClient.getProductsForPage(page, PAGE_SIZE);
            if (responseProducts != null) {
                currentProductsList.addAll(responseProducts.getProductsList());
                loadedProductsSubject.onNext(currentProductsList);
            } else {
                loadedProductsSubject.onNext(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            loadedProductsSubject.onNext(null);
        }

    }

    public Observable<List<Product>> getLoadedProductObservables() {
        return loadedProductsSubject.distinct();
    }

    public void loadProductDetailsForId(String id){
        try{
            ProductDetails productDetails = apiClient.getProductForId(id);
            if(productDetails != null && productDetails.getProduct() != null){
                productDetailsSubject.onNext(productDetails.getProduct());
            } else {
                productDetailsSubject.onNext(null);
            }
        }catch (Exception e){
            e.printStackTrace();
            productDetailsSubject.onNext(null);
        }
    }

    public Observable<Product> getProductDetailsObs(){
        return productDetailsSubject;
    }

}
