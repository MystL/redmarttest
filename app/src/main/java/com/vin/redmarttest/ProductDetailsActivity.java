package com.vin.redmarttest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.vin.redmarttest.data.Product;

import java.net.MalformedURLException;
import java.net.URL;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by melvin on 15/4/18.
 */

public class ProductDetailsActivity extends AppCompatActivity {

    public static final String PRODUCT_ID_KEY = "product.id";
    private ProductLoaderHelper productLoaderHelper;
    private TextView productPrice;
    private TextView productDesc;
    private TextView productTitle;
    private ImageView productImg;
    private ProgressBar progressbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details_layout);
        productImg = findViewById(R.id.product_details_img);
        productTitle = findViewById(R.id.product_details_title);
        productDesc = findViewById(R.id.product_details_desc);
        productPrice = findViewById(R.id.product_details_price);
        progressbar = findViewById(R.id.product_details_loadingcircle);
        progressbar.setVisibility(View.VISIBLE);

        try {
            ApiClient apiClient = new ApiClient(ProductDetailsActivity.this, new URL(MainActivity.API_ENDPOINT));

            productLoaderHelper = new ProductLoaderHelper(apiClient);
            if (getIntent() != null && getIntent().getExtras() != null) {
                String productId = getIntent().getStringExtra(PRODUCT_ID_KEY);
                loadProductDetails(productLoaderHelper, productId);
            }
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("cannot parse API URL");
        }


    }

    private void loadProductDetails(final ProductLoaderHelper helper, final String productId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                helper.loadProductDetailsForId(productId);
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();

        productLoaderHelper.getProductDetailsObs().observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Product>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(Product product) {
                progressbar.setVisibility(View.GONE);
                if (product == null) {
                    Toast.makeText(getApplicationContext(), "Error loading Data, Please try again later", Toast.LENGTH_SHORT).show();
                    return;
                }

                Picasso.with(getApplicationContext())
                        .load(product.getImages()[0].getFullImageUrl().toString())
                        .into(productImg);

                productTitle.setText(product.getTitle());
                productDesc.setText(product.getDescription());
                productPrice.setText(String.format("%s%s", "$", product.getPricing().getPrice()));

            }
        });
    }
}
