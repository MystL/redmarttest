package com.vin.redmarttest;

import android.test.AndroidTestCase;

import com.vin.redmarttest.data.ProductDetails;
import com.vin.redmarttest.data.Products;
import com.vin.redmarttest.exceptions.ApiException;

import java.net.URL;

/**
 *  Potential Problem with this test, as the Test actually fetches real data from API,
 *  if api fails to return (which was observed quite commonly as of 15th May, the test will fail in comparison
 */
public class ApiIntegrationTest extends AndroidTestCase {

    private ApiClient apiClient;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        apiClient = new ApiClient(getContext(), new URL("https://api.redmart.com"));
    }

    public void testGetProducts() throws ApiException {
        try{
            Products products = apiClient.getProductsForPage(0, 5);
            assertNotNull(products);
            assertFalse(products.getProductsList().isEmpty());
            assertEquals(5, products.getProductsList().size());
            // This test will fail if API updated with new Data for first one,
            // just use this for testing if the fetch and parse correct
            assertEquals("33973", products.getProductsList().get(0).getId());
            assertEquals("Australian Broccoli", products.getProductsList().get(0).getTitle());
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public void testGetProductById(){
        try{
            ProductDetails productDetails = apiClient.getProductForId("33973");

            assertNotNull(productDetails);
            assertEquals("33973", productDetails.getProduct().getId());
            assertEquals("Australian Broccoli", productDetails.getProduct().getTitle());
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
