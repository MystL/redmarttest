package com.vin.redmarttest;

import com.vin.redmarttest.data.Product;
import com.vin.redmarttest.data.Products;
import com.vin.redmarttest.exceptions.ApiException;

import junit.framework.TestCase;

import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import rx.observers.TestSubscriber;

/**
 * Created by melvin on 14/4/18.
 */
public class ProductLoaderHelperTest extends TestCase {

    private ApiClient mockApiClient;
    private ProductLoaderHelper productLoaderHelper;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mockApiClient = Mockito.mock(ApiClient.class);
        productLoaderHelper = new ProductLoaderHelper(mockApiClient);

    }

    public void testLoadNext() throws ApiException {
        List<Product> fakeList = new ArrayList<>();
        TestSubscriber<List<Product>> testSubscriber = new TestSubscriber<>();
        productLoaderHelper.getLoadedProductObservables().subscribe(testSubscriber);

        fakeList.add(TestModels.testProduct1);
        Mockito.when(mockApiClient.getProductsForPage(0, ProductLoaderHelper.PAGE_SIZE)).thenReturn(new Products(new Product[]{TestModels.testProduct1}));
        productLoaderHelper.loadNext(0);
        testSubscriber.getOnNextEvents().contains(fakeList);

        fakeList.add(TestModels.testProduct2);
        Mockito.when(mockApiClient.getProductsForPage(1, ProductLoaderHelper.PAGE_SIZE)).thenReturn(new Products(new Product[]{TestModels.testProduct2}));
        productLoaderHelper.loadNext(1);
        testSubscriber.getOnNextEvents().contains(fakeList);

        fakeList.add(TestModels.testProduct3);
        Mockito.when(mockApiClient.getProductsForPage(2, ProductLoaderHelper.PAGE_SIZE)).thenReturn(new Products(new Product[]{TestModels.testProduct3}));
        productLoaderHelper.loadNext(2);
        testSubscriber.getOnNextEvents().contains(fakeList);
    }

}