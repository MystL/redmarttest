package com.vin.redmarttest;

import android.test.AndroidTestCase;

import com.vin.redmarttest.data.Product;
import com.vin.redmarttest.data.Products;
import com.vin.redmarttest.exceptions.ApiException;

import org.mockito.Mockito;

public class ApiClientTest extends AndroidTestCase{

    private ApiClient.Api mockApi;
    private ApiClient client;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mockApi = Mockito.mock(ApiClient.Api.class);
        client = new ApiClient(mockApi);
    }

    public void testGetProducts_returnsNull(){
        Mockito.when(mockApi.getProductsForPage(0,0)).thenReturn(null);

        assertNull(client.getProductsForPage(0,0));
    }

    public void testGetProducts_valid(){
        Mockito.when(mockApi.getProductsForPage(0,3))
                .thenReturn(new Products(new Product[]{TestModels.testProduct1, TestModels.testProduct2, TestModels.testProduct3}));

        assertNotNull(client.getProductsForPage(0,3));
        assertEquals(3, client.getProductsForPage(0,3).getProductsList().size());
    }

    public void testGetProductForId_returnsNull(){
        Mockito.when(mockApi.getProductForId("id")).thenReturn(null);

        assertNull(client.getProductForId("id"));
    }
}