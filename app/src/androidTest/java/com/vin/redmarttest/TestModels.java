package com.vin.redmarttest;

import com.vin.redmarttest.data.Image;
import com.vin.redmarttest.data.Pricing;
import com.vin.redmarttest.data.Product;

/**
 * Created by melvin on 14/4/18.
 */

public class TestModels {

    public static final Product testProduct1 = new Product("id1","title1","desc1", getPricing(), getTestImages());
    public static final Product testProduct2 = new Product("id2","title2","desc2", getPricing(), getTestImages());
    public static final Product testProduct3 = new Product("id3","title3","desc3", getPricing(), getTestImages());
    public static final Product testProduct4 = new Product("id4","title4","desc4", getPricing(), getTestImages());
    public static final Product testProduct5 = new Product("id5","title5","desc5", getPricing(), getTestImages());

    public static Pricing getPricing(){
        return new Pricing(20.0);
    }

    public static Image[] getTestImages(){
      return new Image[]{new Image(0,0,"name1", 0),
              new Image(0,0,"name2", 1)};
    }

}
